from dcmotor import DCMotor
from time import sleep
import network
import machine
import usocket as socket
import ure
import gc

gc.collect()

# Connect to your Wi-Fi network
ssid = "edo.wifi.d7:b3:38"
password = "edoedoedo"
wifi = network.WLAN(network.STA_IF)
wifi.active(True)
wifi.connect(ssid, password)
while not wifi.isconnected():
    pass
print("Connected to Wi-Fi")

# Define motor control pins for Joint 1
pin1_j1 = machine.Pin(5, machine.Pin.OUT)
pin2_j1 = machine.Pin(17, machine.Pin.OUT)
brake1 = machine.Pin(14, machine.Pin.OUT)  # Brake for Joint 1

# Create an instance of the DC motor for Joint 1
j1 = DCMotor(pin1_j1, pin2_j1)

# Define motor control pins for Joint 2
pin1_j2 = machine.Pin(18, machine.Pin.OUT)
pin2_j2 = machine.Pin(19, machine.Pin.OUT)
brake2 = machine.Pin(21, machine.Pin.OUT)  # Brake for Joint 2

# Create an instance of the DC motor for Joint 2
j2 = DCMotor(pin1_j2, pin2_j2)

# Define motor control pins for Joint 3
pin1_j3 = machine.Pin(22, machine.Pin.OUT)
pin2_j3 = machine.Pin(23, machine.Pin.OUT)
brake3 = machine.Pin(26, machine.Pin.OUT)  # Brake for Joint 3

# Create an instance of the DC motor for Joint 3
j3 = DCMotor(pin1_j3, pin2_j3)

# Define motor control pins for Joint 4
pin1_j4 = machine.Pin(12, machine.Pin.OUT)
pin2_j4 = machine.Pin(14, machine.Pin.OUT)

# Create an instance of the DC motor for Joint 4
j4 = DCMotor(pin1_j4, pin2_j4)

# Define motor control pins for Joint 5
pin1_j5 = machine.Pin(32, machine.Pin.OUT)
pin2_j5 = machine.Pin(33, machine.Pin.OUT)

# Create an instance of the DC motor for Joint 5
j5 = DCMotor(pin1_j5, pin2_j5)

# Define motor control pins for Joint 6
pin1_j6 = machine.Pin(25, machine.Pin.OUT)
pin2_j6 = machine.Pin(26, machine.Pin.OUT)

# Create an instance of the DC motor for Joint 6
j6 = DCMotor(pin1_j6, pin2_j6)

# Define motor control pins for Joint 7
pin1_j7 = machine.Pin(27, machine.Pin.OUT)
pin2_j7 = machine.Pin(14, machine.Pin.OUT)

# Create an instance of the DC motor for Joint 7
j7 = DCMotor(pin1_j7, pin2_j7)

# Define the HTML content for the web server
html = """<!DOCTYPE html>
<html>
<head>
    <title>ESP32 Motor Control</title>
</head>
<body>
    <h1>Motor Control</h1>
    
    <h2>Joint 1</h2>
    <button onmousedown="sendCommand('/joint1_forward_press')" onmouseup="sendCommand('/joint1_stop')">Forward</button>
    <button onmousedown="sendCommand('/joint1_backward_press')" onmouseup="sendCommand('/joint1_stop')">Backward</button>
    <button onclick="sendCommand('/joint1_stop')">Stop</button>

    <h2>Joint 2</h2>
    <button onmousedown="sendCommand('/joint2_forward_press')" onmouseup="sendCommand('/joint2_stop')">Forward</button>
    <button onmousedown="sendCommand('/joint2_backward_press')" onmouseup="sendCommand('/joint2_stop')">Backward</button>
    <button onclick="sendCommand('/joint2_stop')">Stop</button>

    <h2>Joint 3</h2>
    <button onmousedown="sendCommand('/joint3_forward_press')" onmouseup="sendCommand('/joint3_stop')">Forward</button>
    <button onmousedown="sendCommand('/joint3_backward_press')" onmouseup="sendCommand('/joint3_stop')">Backward</button>
    <button onclick="sendCommand('/joint3_stop')">Stop</button>

    <h2>Joint 4</h2>
    <button onmousedown="sendCommand('/joint4_forward_press')" onmouseup="sendCommand('/joint4_stop')">Forward</button>
    <button onmousedown="sendCommand('/joint4_backward_press')" onmouseup="sendCommand('/joint4_stop')">Backward</button>
    <button onclick="sendCommand('/joint4_stop')">Stop</button>

    <h2>Joint 5</h2>
    <button onmousedown="sendCommand('/joint5_forward_press')" onmouseup="sendCommand('/joint5_stop')">Forward</button>
    <button onmousedown="sendCommand('/joint5_backward_press')" onmouseup="sendCommand('/joint5_stop')">Backward</button>
    <button onclick="sendCommand('/joint5_stop')">Stop</button>

    <h2>Joint 6</h2>
    <button onmousedown="sendCommand('/joint6_forward_press')" onmouseup="sendCommand('/joint6_stop')">Forward</button>
    <button onmousedown="sendCommand('/joint6_backward_press')" onmouseup="sendCommand('/joint6_stop')">Backward</button>
    <button onclick="sendCommand('/joint6_stop')">Stop</button>

    <h2>Joint 7</h2>
    <button onmousedown="sendCommand('/joint7_forward_press')" onmouseup="sendCommand('/joint7_stop')">Forward</button>
    <button onmousedown="sendCommand('/joint7_backward_press')" onmouseup="sendCommand('/joint7_stop')">Backward</button>
    <button onclick="sendCommand('/joint7_stop')">Stop</button>

    <script>
        function sendCommand(command) {
            fetch(command)
                .then(response => response.text())
                .then(data => console.log(data))
                .catch(error => console.error('Error:', error));
        }
    </script>
</body>
</html>
"""

# Create a function to handle incoming requests
def handle_request(client):
    request = client.recv(1024)
    command_match = ure.search(b'GET /(\w+)', request)

    if command_match:
        command = command_match.group(1).decode()
        if command == 'joint1_forward_press':
            j1.forward(100)
            brake1.on()  # Set brake for Joint 1
        elif command == 'joint1_backward_press':
            j1.backwards(100)
            brake1.on()  # Set brake for Joint 1
        elif command == 'joint1_stop':
            j1.stop()
            brake1.off()  # Release brake for Joint 1

        elif command == 'joint2_forward_press':
            j2.forward(100)
            brake2.on()  # Set brake for Joint 2
        elif command == 'joint2_backward_press':
            j2.backwards(100)
            brake2.on()  # Set brake for Joint 2
        elif command == 'joint2_stop':
            j2.stop()
            brake2.off()  # Release brake for Joint 2

        elif command == 'joint3_forward_press':
            j3.forward(100)
            brake3.on()  # Set brake for Joint 3
        elif command == 'joint3_backward_press':
            j3.backwards(100)
            brake3.on()  # Set brake for Joint 3
        elif command == 'joint3_stop':
            j3.stop()
            brake3.off()  # Release brake for Joint 3

        elif command == 'joint4_forward_press':
            j4.forward(100)
        elif command == 'joint4_backward_press':
            j4.backwards(100)
        elif command == 'joint4_stop':
            j4.stop()

        elif command == 'joint5_forward_press':
            j5.forward(100)
        elif command == 'joint5_backward_press':
            j5.backwards(100)
        elif command == 'joint5_stop':
            j5.stop()

        elif command == 'joint6_forward_press':
            j6.forward(100)
        elif command == 'joint6_backward_press':
            j6.backwards(100)
        elif command == 'joint6_stop':
            j6.stop()

        elif command == 'joint7_forward_press':
            j7.forward(100)
        elif command == 'joint7_backward_press':
            j7.backwards(100)
        elif command == 'joint7_stop':
            j7.stop()

    response = "HTTP/1.1 200 OK\r\nContent-Length: {}\r\n\r\n{}".format(len(html), html)
    client.send(response)
    client.close()

# Create a socket and start the server
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('', 80))
server_socket.listen(5)
print("Server listening on port 80")

# Add these lines at the end of the code
ip_address = wifi.ifconfig()[0]
print("Server running at http://{}".format(ip_address))

while True:
    client, addr = server_socket.accept()
    handle_request(client)
